const colors = require('tailwindcss/colors')
module.exports = {
  ...((process.env.NODE_ENV === 'production')?{
    mode: 'jit',
    purge: ['index.html']
    // specify other options here
  }:{}),
  theme:{
    extend:{
      colors:{
        'primary':'#8c1d00',
        'secondary':'#91764b'
      }
    }
  }
  
}